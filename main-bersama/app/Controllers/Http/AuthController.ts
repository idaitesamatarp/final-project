import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
    /**
     * @swagger
     * /api/v1/register:
     *   post:
     *     tags:
     *       - Authentification
     *     requestBody: 
     *       required: true
     *       content: 
     *          application/json:
     *              schema:
     *                  $ref: '#definitions/User'
     *          application/x-www-form-urlencoded:
     *              schema: 
     *                  $ref: '#definitions/User'
     *     responses:
     *       200:
     *         data: Send hello message
     *         example:
     *           message: Success
     */
    
    public async register({ request, response }: HttpContextContract) {
        const userValid = await request.validate(UserValidator)
        const user = await User.create(userValid)
        let digits = '0123456789'
        let otp = ''

        try {
            for (let i = 0; i < 4; i++ ) {
                otp += digits[Math.floor(Math.random() * 10)];
            }

            await Mail.send((message) => {
                message
                    .from('nikita@example.com')
                    .to(user.email)
                    .subject('Welcome Onboard!')
                    .htmlView('email/otp_verification', {
                        kode_otp: otp,
                        user: { name: user.name }
                    })
            })

            await Database
                .insertQuery()
                .table('otp_codes')
                .insert({ otp_code: otp, user_id: user.id })
            
            return response.created({message: 'Register Success! Please verify your OTP Code!', data: user})
        } catch (error) {
            console.log(error)
            return response.unprocessableEntity({ message: error.messages })
        }
    }

    /**
     * otpConfirmation
     */
    public async otpConfirmation({request, response}: HttpContextContract) {
        const email = request.input('email')
        const otp_code = request.input('otp_code')
        const user = await User.findBy('email', email)
        let checkOtp = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

        try {
            if (user?.id == checkOtp.user_id) {
                user!.is_verified = true
                user?.save()
                return response.ok({message: 'Successfully Confirmed OTP'})
            }    
        } catch (error) {
            return response.badRequest({message: 'OTP Verification Failed'})
        }
    }

    /**
     * @swagger
     * /api/v1/login
     * 
     */
     public async login({ auth, request, response }: HttpContextContract) {
        const userSchema = schema.create({
            email: schema.string(),
            password: schema.string()
        })
        
        try {
            await request.validate({ schema: userSchema })
            const email = request.input('email')
            const password = request.input('password')
            if (auth.user?.is_verified == false) {
                return response.unauthorized({message: 'Not yet Verification!'})
            } else {
                const createToken = await auth.use('api').attempt(email, password, {
                    expiresIn: '10hours'
                })
                return response.ok({message: 'Login Success!', token: createToken.token})
            }
        } catch (error) {
            if (error.guard) {
                return response.badRequest({ message: 'Error! Invalid Email or Password', error: error.message })
            } else {
                return response.badRequest({ message: 'Error! Invalid Email or Password', error: error.messages })
            }
        }
    }

}
