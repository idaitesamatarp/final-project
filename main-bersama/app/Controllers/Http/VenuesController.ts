import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Venue from 'App/Models/Venue'
import FieldValidator from 'App/Validators/FieldValidator'
import VenueValidator from 'App/Validators/VenueValidator'

export default class VenuesController {
  /**
  * @swagger
  * /api/v1/venues:
  *   get:
  *     tags:
  *       - Venues
  *     summary: Lihat Venues
  *     responses:
  *       200:
  *         data: Send hello message
  *         example:
  *           message: Success
  */
  public async index({ response }: HttpContextContract) {
    const venue = await Venue.all()
    return response.ok({message: 'Success', data: venue})
  }

  public async create ({}: HttpContextContract) {
  }

  public async store({ auth, request, response }: HttpContextContract) {
    try {
      const authUser = auth.user
      const data = await request.validate(VenueValidator)
      const newVenue = await authUser?.related('venues').create(data)
      console.log(newVenue!.user_id == authUser!.id) // true

      return response.status(201).json({message: 'Created New Venue!'})
    } catch (error) {
      return response.badRequest({message: error.messages})
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const venueId = params.id
      const venue = await Venue.findBy('id', venueId)
          
      return response.ok({ message: 'Success', data: venue })
    } catch (error) {
      return response.badRequest({message: error.messages})
    }
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update({ params, request, response }: HttpContextContract) {
    try {
      const venueId = params.id
      const data = await request.validate(VenueValidator)
      const venue = await Venue.findOrFail(venueId)
      await venue
        .merge(data)
        .save()
      return response.ok({message: "Update Success"})
    } catch (error) {
      return response.badRequest({message: error.messages})
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      const venueId = params.id
      const venue = await Venue.findOrFail(venueId)
      await venue.delete()
      return response.ok({message: "Delete Success"})
    } catch (error) {
      return response.badRequest({message: error.messages})
    }
  }

  /**
   * fields
   */
  public async createfield({ auth, params, request, response }: HttpContextContract) {
    try {
      const authUser = auth.user?.id
      const data = await request.validate(FieldValidator)
      const venue_id = params.id
      const venue = await Venue.findOrFail(venue_id)
      if (authUser == venue.user_id) {
        const field = await venue?.related('fields').create(data)
        return response.status(201).json({message: 'Created New Venue!', field})
      }
    } catch (error) {
      return response.badRequest({message: error.messages})
    }
  }
}
