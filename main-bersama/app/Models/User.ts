import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'
import Venue from 'App/Models/Venue'

export default class User extends BaseModel {
  /** 
*  @swagger
*  definitions:
*    User:
*      type: object
*      properties:
*        name:
*          type: string
*        email:
*          type: string
*        password:
*          type: string
*        role: 
*          type: string
*      required:
*        - name
*        - email
*        - password
*        - role
*/
  
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public role: string

  @column()
  public is_verified: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @hasMany(() => Venue, {
    foreignKey: 'user_id', // defaults to userId
  })
  public venues: HasMany<typeof Venue>
}
