import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Acl {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>, rule: string[]) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const roles = rule
    if (roles.length == 0) {
      await next()
    } else {
      try {
        const user = await auth.user
        const role = user!.role
        if(roles.includes(role)){
          await next()
        } else {
          return response.badRequest({ message: `Only user with role: ${roles} can access the route` })
        }
      } catch (e) {
        console.log(e)
        return response.badRequest({ message: `Only user with role: ${roles} can access the route` })
      }
    }
  }
}
